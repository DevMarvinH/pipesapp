import { Component } from '@angular/core';
import { interval } from 'rxjs';

@Component({
	selector: 'app-no-comunes',
	templateUrl: './no-comunes.component.html',
	styles: [
	]
})
export class NoComunesComponent {

	//i18nSelect
	nombre: string = 'Marvin';
	genero: string = 'masculino';

	invitacionMapa = {
		'masculino': 'invitarlo',
		'femenino': 'invitarla'
	};

	//i18nPlural
	clientes: string[] = ['Maria', 'Pedro', 'Eduardo', 'Claudia', 'Felipe'];
	clientesMapa = {
		'=0': 'no tenemos ningún cliente esperando',
		'=1': 'tenemos un cliente esperando',
		'other': 'tenemos # clientes esperando'
	};

	cambiarCliente(){
		switch (this.genero) {
			case 'femenino':
				this.genero = 'masculino';
				this.nombre = 'Marvin';
				break;
			case 'masculino':
				this.genero = 'femenino';
				this.nombre = 'Susana';
				break;
		}
	}

	borrarCliente(){
		if (this.clientes.length > 0) {
			this.clientes.pop();
		}
	}

	//KeyValue Pipe
	persona = {
		nombre: 'Marvin',
		edad: 25,
		dirección: 'Izalco, Sonsonate'
	};

	//Async Pipe
	miObservable = interval(1000);

	valorPromesa = new Promise((resolve, reject) => {
		setTimeout(() => resolve('Tenemos data de promesa'), 3500)
	});
	
}
