import { Component } from '@angular/core';

@Component({
	selector: 'app-basicos',
	templateUrl: './basicos.component.html',
	styles: [
	]
})
export class BasicosComponent {

	nombreLower: string = 'marvin';
	nombreUpper: string = 'MARVIN';
	nombreCompleto: string = 'mArviN hErNÁNdez';

	fecha: Date = new Date();
}
